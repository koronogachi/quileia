<%-- 
    Document   : registro_ciudad
    Created on : 30-jun-2019, 20:59:49
    Author     : Javier
--%>

<div class="header">
    <h2>Registro Ciudad</h2>
</div>
<div class="body">
    <div class="row clearfix">
        <div class="col-sm-6">
            <form method="POST" action="">
                <div class="form-group form-float">
                    <h2 class="card-inside-title">Nombre del ciudad</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="ciudad" required>
                        <label class="form-label">Nombre del ciudad</label>
                    </div>
                    <h2 class="card-inside-title">Cantidad de habitantes</h2>
                    <div class="form-line">
                        <input type="number" class="form-control" name="habitantes" required>
                        <label class="form-label">Cantidad de habitantes</label>
                    </div>
                    <h2 class="card-inside-title">Sitio Turistico</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="sitio" required>
                        <label class="form-label">Sitio Turistico</label>
                    </div>
                    <h2 class="card-inside-title">Hotel Mas Reservado</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="hotel" required>
                        <label class="form-label">Hotel Mas Reservado</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>