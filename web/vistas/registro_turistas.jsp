<%-- 
    Document   : registro_turistas
    Created on : 30-jun-2019, 20:47:34
    Author     : Javier
--%>

<div class="header">
    <h2>Registro Turista</h2>
</div>
<div class="body">
    <div class="row clearfix">
        <div class="col-sm-6">
            <form method="POST" action="">
                <div class="form-group form-float">
                    <h2 class="card-inside-title">Nombre Completo</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="turista" required>
                        <label class="form-label">Nombre Completo</label>
                    </div>
                    <h2 class="card-inside-title">Fecha Nacimiento</h2>
                    <div class="form-line">
                        <input type="date" class="form-control" name="fecha_nacimiento" required>
                    </div>
                    <h2 class="card-inside-title">Identificacion</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="identificacion" required>
                        <label class="form-label">Identificacion</label>
                    </div>
                    <h2 class="card-inside-title">Tipo Identificacion</h2>
                    <div class="form-line">
                        <input type="text" class="form-control" name="tipo_identificacion" required>
                        <label class="form-label">Tipo Identificacion</label>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

